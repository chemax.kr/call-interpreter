const log4js = require('log4js');
const lg = log4js.getLogger();
lg.level = process.env.LOGLEVEL || 'debug';
module.exports = lg;