var EventEmitter = require('events')
var util = require('util');
var configuration = require('config');
var ts = require('tail-stream');
var Speech = require('@google-cloud/speech');

function Transcripter(soundPath, request) {

  this.tsConfig = configuration.get('tailstream');
  this.speech = Speech(configuration.get('google.API'));

  this.soundPath = soundPath;
  this.language = request.config.languageCode;

  EventEmitter.call(this);
  var self = this;
  this.startTranscription = function() {
    var recognizeStream = this.speech.streamingRecognize(request)
    .on('error', function (error) {
      self.emit('error', error);
    })
    .on('data', function (data) {
       if (data.results[0] && data.results[0].alternatives[0]) {
         self.emit('result', data.results[0].alternatives[0].transcript, data.results[0].alternatives[0].confidence, data.results[0].isFinal);
       }
       else {
         self.emit('limit', data)
       }
    });

    var tstream = ts.createReadStream(this.soundPath, this.tsConfig);
    tstream.on('error', function (error) {
       self.emit('error', error);
    })
    tstream.pipe(recognizeStream);
  };

};

util.inherits(Transcripter, EventEmitter)

module.exports = Transcripter;
